var express = require('express');
var path = require('path');
var favicon = require('serve-favicon');
var logger = require('morgan');
var cookieParser = require('cookie-parser');
var bodyParser = require('body-parser');
var fs = require('fs');
var cors = require('cors')


var index = require('./routes/index');
var clause = require('./routes/clause');

var app = express();

// view engine setup
app.set('views', path.join(__dirname, 'views'));
app.set('view engine', 'pug');

// uncomment after placing your favicon in /public
//app.use(favicon(path.join(__dirname, 'public', 'favicon.ico')));
app.use(logger('dev'));
app.use(cors());
app.use(bodyParser.json());
app.use(bodyParser.urlencoded({ extended: false }));
app.use(cookieParser());
app.use(express.static(path.join(__dirname, 'public')));

app.use('/', index);
app.use('/clause', clause);

app.post('/', function(req, res) {
    var content = req.body;
    console.log('CLAUSE POST: ');
    console.log(content);
});

app.post('/clause', function(req, res) {
    var content = req.body;
    console.log('POST:');
    console.log(content);

    saveClauseToPublicFolder(content, function(err) {
        if (err) {
            console.log('SAVE Error: ' + err);
            return;
        }
    });
});

// catch 404 and forward to error handler
app.use(function(req, res, next) {
    var err = new Error('Not Found');
    err.status = 404;
    next(err);
});

// error handler
app.use(function(err, req, res, next) {
    // set locals, only providing error in development
    res.locals.message = err.message;
    res.locals.error = req.app.get('env') === 'development' ? err : {};

    // render the error page
    res.status(err.status || 500);
    res.render('error');
});

function saveClauseToPublicFolder(content, callback){
    var fs = require('fs');
	var name = content.Client;
	var term = '[' + JSON.stringify(content) + ']';
    fs.writeFile('./public/clauses/'+name+'.json', term, callback);
}

module.exports = app;

console.log('well...');

Office.initialize = function(reason) {
    // Checks for the DOM to load using the jQuery ready function.
    console.log('loaded...');
    $(document).ready(function() {
        $('#creator').submit(function() {
            console.log('a');
            buildTerm();
            return false;
        });
        $('#grabber').submit(function() {
            console.log('2');
            getText();
            return false;
        });
    });
}