var express = require('express');
var app = express();
var router = express.Router();
var fs = require('fs');

app.use(express.static('public'));

/* GET users listing. */
router.get('/', function(req, res, next) {
  res.render('clause', { title: 'Clause Creator' });
});

module.exports = router;
