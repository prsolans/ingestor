//The initialize function is required for all apps.


console.log('well...');

Office.initialize = function(reason) {
    // Checks for the DOM to load using the jQuery ready function.
    console.log('loaded...');
    $(document).ready(function() {
        $('#creator').submit(function() {
            console.log('a');
            buildTerm();
            return false;
        });
        $('#grabber').submit(function() {
            console.log('2');
            getText();
            return false;
        });
    });
}


function getText() {
    console.log('3');
    Office.context.document.getSelectedDataAsync(Office.CoercionType.Text, function(asyncResult) {
        if (asyncResult.status == Office.AsyncResultStatus.Failed) {
            write('Action failed. Error: ' + asyncResult.error.message);
            console.log('Failed');
        } else {
            write(asyncResult.value, 'Clause');
            console.log('Success');
            console.log(asyncResult.value);
        }
    });
    console.log('3-end');
}

function buildTerm() {
    var content = $('#creator').serializeArray().reduce(function(a, x) {
        a[x.name] = x.value;
        return a;
    }, {});

    console.log('BUILD: ' + JSON.stringify(content));

    $.ajax({
        url: 'http://127.0.0.1:3000/clause',
        contentType: "application/json; charset=utf-8",
        data: JSON.stringify(content),
        type: 'POST',
        success: function(response) {
            console.log('Success');
        },
        error: function(error) {
            console.log('Error:' + error);
        }
    });
}

// Function that writes to a div with id='message' on the page.
function write(message, location) {
    if (location == 'Clause') { document.getElementById(location).value += message; } else { document.getElementById(location).innerHTML += message; }
}